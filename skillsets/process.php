<?php

ini_set('display_errors', 1);

error_reporting(E_ALL);

?>
<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Thu, 07-07-16, 13:45:57 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="My online portfolio that illustrates skills aquired in various courses at Florida State University.">
    <meta name="author" content="Kyle Gross">
    <link rel="icon" href="favicon.ico">

    <title>lis4381 ~ Simple Employee Class</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Note: following file is for form validation. -->
<link rel="stylesheet" href="css/formValidation.min.css"/>

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<?php include_once("../global/nav.php"); ?>

<div class="container-fluid">
         <div class="starter-template">
                        <div class="page-header">
                            <?php include_once("global/header.php"); ?> 
                        </div>


    <?php
    require_once("classes/person.php");
    


    $personFname = $_POST['fname'];
    $personLname = $_POST['lname'];
    $personAge = $_POST['age'];



    $person1 = new Person();
    $person2 = new Person($personFname, $personLname, $personAge);

    ?>


    <h2>Simple Person Class</h2>

    <div class="table-responsive">
        <table id="myTable" class="table table-striped table-condensed">
            <thead>
                <tr>
                    <th>FName</th>
                    <th>LName</th>
                    <th>Age</th>

                </tr>
            </thead>
            <tr>
                <td><?php echo $person1->GetFname(); ?></td>  
                <td><?php echo $person1->GetLname(); ?></td> 
                <td><?php echo $person1->GetAge(); ?></td> 
            </tr>

            <tr>
                <td><?php echo $person2->GetFname(); ?></td>  
                <td><?php echo $person2->GetLname(); ?></td> 
                <td><?php echo $person2->GetAge(); ?></td> 

            </tr>
        </table>

        <?php
include_once "global/footer.php";
?>

</div>
</div>

<!-- Bootstrap JavaScript
    ================================================== -->
    <!-- Placed at end of document so pages load faster -->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

            <script>
                $(document).ready(function(){
                    $('#myTable').DataTable({
                        responsive: true
                    });
                });
            </script>







</body>
</html>