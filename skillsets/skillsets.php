<!DOCTYPE html>
<html lang="en">
<head>
<!--
"Time-stamp: <Thu, 07-07-16, 13:45:57 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="My online portfolio that illustrates skills aquired in various courses at Florida State University.">
    <meta name="author" content="Kyle Gross">
    <link rel="icon" href="favicon.ico">

    <title>lis4381 ~ Skillsets</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Note: following file is for form validation. -->
<link rel="stylesheet" href="css/formValidation.min.css"/>

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<?php include_once("../global/nav.php"); ?>

    <div class="container">
        <div class="starter-template">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    
                    <div class="page-header">
                        <?php include_once("global/header.php"); ?> 
                    </div>


<h2>Simple Person Class</h2>

                        <form class="form-horizontal" role="form" method="post" action="process.php">
                                
                                <div class="form-group">
                                        <label class="col-sm-2 control-label" for="type">FName:</label>
                                        <div class="col-sm-10">
                                                <input type="text" class="form-control" name="fname" id="fname" placeholder="Enter first name">
                                        </div>
                                </div>

                                <div class="form-group">
                                        <label class="col-sm-2 control-label" for="length">LName:</label>
                                        <div class="col-sm-10">
                                                <input type="text" class="form-control" name="lname" id="lname" placeholder="Enter last name">
                                        </div>
                                </div>

                                <div class="form-group">
                                        <label class="col-sm-2 control-label" for="length">Age:</label>
                                        <div class="col-sm-10">
                                                <input type="text" class="form-control" name="age" id="age" placeholder="Enter age">
                                        </div>
                                </div>

                                <div class="form-group">
                                        <div class="col-sm-12">
                                                <button type="submit" class="btn btn-primary" name="signup" value="Sign up">Submit</button>
                                        </div>
                                </div>

                        </form>


<?php
include_once "global/footer.php";
?>



<!-- Bootstrap JavaScript
    ================================================== -->
    <!-- Placed at end of document so pages load faster -->
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="js/ie10-viewport-bug-workaround.js"></script>

            <script>
                $(document).ready(function(){
                    $('#myTable').DataTable({
                        responsive: true
                    });
                });
            </script>

</body>
</html>
