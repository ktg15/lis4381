> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**

# LIS 4381 - Mobile Web Application Development

## Kyle Gross

### LIS 4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Install JDK
    - Install Android Studio and create My First App
    - Provide screenshots of installation
    - Create Bitbucket Repo
    - Complete Bitbucket Tutorials (bitbucketstationlocations and myteamquotes)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Screenshot of running application's first user interface
    - Screenshot of running application's second user interface

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Screenshot of ERD 
    - Screenshot of My Event Application
	- Links to ERD files
	- Chapter Questions
    
4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Screenshot of Business Card 1st Interface
    - Screenshot of Business Card 2nd Interface

5. [A4 README.md](a4/README.md "My A4 README.me file")
    - Screenshot of passed validation
    - Screenshot of failed validation
    - Screenshot of my online portfolio homepage

6. [A5 README.md](a5/README.md "My A5 README.me file")
    - Screenshot of MYSQL database
    - Screenshot of Error screen
    - Chapter Questions

7. [P2 README.md](p2/README.md "My P2 README.me file")
    - Screenshot of index.php
    - Screenshot of edit_petstore.php
    - Screenshot of Error screen
    - Screenshot of carousel
    - Chapter Questions





