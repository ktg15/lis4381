
# LIS 4381 Mobile Web Application Development

## Kyle Gross

### Project #2 Requirements:

*Three Parts:*

1. Screenshot of index.php
2. Screenshot of edit_petstore.php
3. Screenshot of edit_petstore_process.php
4. Screenshot Error screen
5. Chapter Questions

#### README.md file should include the following items:


#### Assignment Screenshots:

*Screenshot of index:

![INDEX](img/index.png)

*Screenshot of edit:

![EDIT](img/edit.png)

*Screenshot of error:

![ERROR](img/error.png)

*Screenshot of rss feed:

![RSS](img/rss.png)

*Screenshot of carousel:

![CAROUSEL](img/carousel.png)




