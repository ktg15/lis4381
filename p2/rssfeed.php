<!DOCTYPE HTML>
<html>
<head> 
    <meta charset="utf-8">
    <title>Using RSS Feeds</title> 
</head> 

<body>
<?php 
//What is RSS? http://whatisrss.com/
//RSS Specification: http://www.rss-specifications.com/rss-specifications.htm
//Anatomy of an RSS feed: http://www.webreference.com/authoring/languages/xml/rss/feeds/index.html
//tutorial: http://www.htmlgoodies.com/beyond/xml/article.php/3691751

//example:
//$url = 'http://feeds.bbci.co.uk/news/world/rss.xml';
$url = 'https://krebsonsecurity.com/feed/';
$feed = simplexml_load_file($url, 'SimpleXMLIterator');
echo "<h2>" .$feed->channel->description ."</h2><ol>";

$filtered = new LimitIterator($feed->channel->item, 0, 10);
foreach ($filtered as $item) { ?>
    <h4><li><a href="<?php echo $item->link; ?>" target="_blank"><?php echo $item->title;?></a></li></h4>
    <?php 
    //must set default time zone
    date_default_timezone_set('America/New_York');

    $date = new DateTime($item->pubDate);
    $date->setTimezone(new DateTimeZone('America/New_York'));
    $offset = $date->getOffset();
    $timezone = ($offset == -14400) ? ' EDT' : ' EST';
    //  echo $date->format('m/d/y, g:ia') .$timezone;
    echo $date->format('M j, Y, g:ia') .$timezone;
    ?>
        <p><?php echo $item->description; ?></p> 
    <?php } ?>
        <!-- end ordered list -->
        </ol> 
    </body> 
</html> 