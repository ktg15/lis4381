
# LIS 4381 Mobile Web Application Development

## Kyle Gross

### Assignment #3 Requirements:

*Three Parts:*

1. Screenshot of ERD 
2. Screenshot of My Event Application
3. Links to ERD files
4. Chapter Questions

#### README.md file should include the following items:


#### Assignment Screenshots:

*Screenshot of ERD:

![ERD](img/erd.png)

*Screenshot of 1st Interface:

![1st Interface](img/1.png)

*Screenshot of 2nd Interface:

![2nd Interface](img/2.png)

*Links to ERD Files:

1. [a3.mwb](links/a3.mwb "a3.mwb")

2. [a3.sql](links/a3.sql "a3.sql")
