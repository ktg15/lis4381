
# LIS 4381 Mobile Web Application Development

## Kyle Gross

### Assignment #4 Requirements:

*Three Parts:*

1. Screenshot of Passed Validation
2. Screenshot of Failed Validation
3. Screenshot of My Online Portfolio Homepage
4. Chapter Questions

#### README.md file should include the following items:


#### Assignment Screenshots:

*Screenshot of Passed Validation:

![PASSED](img/passed.png)

*Screenshot of Failed Validation:

![FAILED](img/failed.png)

*Screenshot of Homepage:

![HOMEPAGE](img/homepage.png)


