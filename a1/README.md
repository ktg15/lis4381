
# LIS 4381 Mobile Web Application Development

## Kyle Gross

### Assignment #1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Instructions
3. Chapter Questions (Ch. 1, 2)

#### README.md file should include the following items:

* Screenshot of AMPPS Installation.
* Screenshot of running java Hello.
* Screenshot of running Android Studio My First App.
* git commands with short descriptions.


> #### Git commands w/short descriptions:

1. git init-Create a new local repository
2. git status-List the files you've changed and those you still need to add or commit
3. git add-Add one or more files to staging
4. git commit-Commit any new files you've added with 'git add'
5. git push-Send changes to the master branch of your remote repository
6. git pull-Fetch and merge changes on the remote server to your working directory
7. git diff-View all the merge conflicts

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/php.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/helloWorld.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/ktg15/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/ktg15/myteamquotes/ "My Team Quotes Tutorial")
